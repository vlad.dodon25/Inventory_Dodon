package com.netcracker.edu.inventory.model.impl;

import com.netcracker.edu.inventory.exception.DeviceValidationException;
import com.netcracker.edu.inventory.model.Device;
import com.netcracker.edu.inventory.model.Rack;
import com.netcracker.edu.inventory.service.impl.ServiceImpl;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by vladd on 20.03.2017.
 */
public class RackArrayImpl implements Rack, Serializable {

    protected static Logger LOGGER = Logger.getLogger(RackArrayImpl.class.getName());

    protected final Class typeOfDevises;
    protected Device devices[];

    public RackArrayImpl(int size, Class typeOfDevises) {
        if (typeOfDevises == null) {
            IllegalArgumentException e = new IllegalArgumentException("Type cannot be null");
            LOGGER.log(Level.SEVERE,e.getMessage(),e);
            throw e;
        }

        if (size < 0) {
            IllegalArgumentException e = new IllegalArgumentException("Rack size should not be negative");
            LOGGER.log(Level.SEVERE,e.getMessage(),e);
            throw e;
        }
        devices = new Device[size];
        this.typeOfDevises = typeOfDevises;
    }

    public RackArrayImpl(int size) {
        this(size,Device.class);
    }


    @Override
    public int getSize() {
        return devices.length;
    }

    @Override
    public int getFreeSize() {
        int freeSize = 0;
        for (int i = 0; i < devices.length; i++){
            if (devices[i] == null) {
                freeSize++;
            }
        }
        return freeSize;
    }

    @Override
    public Class getTypeOfDevices() {
        return typeOfDevises;
    }

    @Override
    public Device getDevAtSlot(int index) {
        isIndexOutOfBounds(index);

        return devices[index];
    }

    @Override
    public boolean insertDevToSlot(Device device, int index) {
        isIndexOutOfBounds(index);

        if (!new ServiceImpl().isValidDeviceForInsertToRack(device)) {
            DeviceValidationException e = new DeviceValidationException("Rack.insertDevToSlo",device);
            LOGGER.log(Level.SEVERE,e.getMessage(),e);
            throw e;
        }

        if (!typeOfDevises.isAssignableFrom(Device.class) && !typeOfDevises.isInstance(device)) {
            IllegalArgumentException e = new  IllegalArgumentException("The type of the transmitted object is incompatible with the type that the rack can store");
            LOGGER.log(Level.SEVERE,e.getMessage(),e);
            throw e;
        }

        if (devices[index] == null) {
            devices[index] = device;
            return true;
        } else {
            LOGGER.log(Level.INFO,"Can not be inserted into this slot <" + index + ">, the slot is busy");
            return false;
        }
    }

    @Override
    public Device removeDevFromSlot(int index) {
        isIndexOutOfBounds(index);

        if (devices[index] == null) {
            LOGGER.log(Level.INFO,"Can not remove from empty slot <" + index + ">");
            return null;
        } else {
            Device device = devices[index];
            devices[index] = null;

            return device;
        }
    }

    @Override
    public Device getDevByIN(int in) {
        Device device = null;
        if (in >= 0) {
            for (Device device1 : devices) {
                if (device1 != null && device1.getIn() == in) {
                    device = device1;
                }
            }
        }
        return device;
    }

    @Override
    public Device[] getAllDeviceAsArray() {
        Device[] dev = new Device[devices.length-getFreeSize()];
        int j = 0;

        for (int i = 0; i < devices.length; i++ ){
            if (devices[i] != null) {
                dev[j] = devices[i];
                j++;
            }
        }
        return dev;
    }

    private void isIndexOutOfBounds(int index) {
        if (index < 0 || index >= devices.length) {
            IndexOutOfBoundsException e = new IndexOutOfBoundsException("Invalid array index " + index + ", allowable range from 0 to "
                    + (devices.length - 1));
            LOGGER.log(Level.SEVERE,e.getMessage(),e);
            throw e;
        }
    }
}
