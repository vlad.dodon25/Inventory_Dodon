package com.netcracker.edu.inventory.model.impl;

import com.netcracker.edu.inventory.model.Device;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by vladd on 20.03.2017.
 */
abstract class AbstractDevice implements Device, Serializable{

    protected int in = 0;
    protected String type;
    protected String manufacturer;
    protected String model;
    protected Date productionDate;

    protected static Logger LOGGER = Logger.getLogger(AbstractDevice.class.getName());

    @Override
    public int getIn() {
        return in;
    }

    @Override
    public void setIn(int in) {
        if (in < 0) {
            IllegalArgumentException e = new IllegalArgumentException("IN can not be negative, Device: " + this.getClass());
            LOGGER.log(Level.SEVERE, e.getMessage(),e);
            throw e;
        }
        if (this.in != 0) {
            LOGGER.log(Level.INFO, "Inventory number can not be reset");
        } else {
            this.in = in;
        }
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getManufacturer() {
        return manufacturer;
    }

    @Override
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public Date getProductionDate() {
        return productionDate;
    }

    @Override
    public void setProductionDate(Date productionDate) {
        this.productionDate = productionDate;
    }
}
