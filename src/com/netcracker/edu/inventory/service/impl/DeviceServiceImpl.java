package com.netcracker.edu.inventory.service.impl;

import com.netcracker.edu.inventory.exception.DeviceValidationException;
import com.netcracker.edu.inventory.model.Device;
import com.netcracker.edu.inventory.model.impl.Battery;
import com.netcracker.edu.inventory.model.impl.Router;
import com.netcracker.edu.inventory.model.impl.Switch;
import com.netcracker.edu.inventory.model.impl.WifiRouter;
import com.netcracker.edu.inventory.service.DeviceService;
import com.sun.xml.internal.ws.util.ReadAllStream;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.print.DocFlavor;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by vladd on 17.04.2017.
 */
public class DeviceServiceImpl implements DeviceService {

    protected enum Params { TYPE, MANUFACTURER, MODEL }

    protected static final String STRING_NULL = "\n";

    protected static final String STRING_DELIMITER = "|";

    protected static Logger LOGGER = Logger.getLogger(DeviceServiceImpl.class.getName());

    private static Comparator<Device> comparatorByIn = new  Comparator<Device>() {
        @Override
        public int compare(Device d1, Device d2) {
            if (d1 == null || d2 == null) {
                if (d1 == d2)    { return  0; }
                if (d1 == null)  { return  1; }
                else             { return -1; }
            } else {
                if (d1.getIn() == 0 || d2.getIn() == 0) {
                    if (d1.getIn() == d2.getIn())   { return  0; }
                    if (d1.getIn() == 0)            { return  1; }
                    else                            { return -1; }
                } else {
                    return Integer.compare(d1.getIn(),d2.getIn());
                }
            }
        }
    };

    private static Comparator<Device> comparatorByProductionDate = new  Comparator<Device>() {
        @Override
        public int compare(Device d1, Device d2) {
            if (d1 == null || d2 == null) {
                if (d1 == d2)    { return  0; }
                if (d1 == null)  { return  1; }
                else             { return -1; }
            } else {
                if (d1.getProductionDate() == null || d2.getProductionDate() == null) {
                    if (d1.getProductionDate() == d2.getProductionDate())   { return  0; }
                    if (d1.getProductionDate() == null)                     { return  1; }
                    else                                                    { return -1; }
                } else {
                    return d1.getProductionDate().compareTo(d2.getProductionDate());
                }
            }
        }
    };

    @Override
    public void sortByIN(Device[] devices) {
        if (devices != null) {
            Arrays.sort(devices,comparatorByIn);
        }
    }

    @Override
    public void sortByProductionDate(Device[] devices) {
        if (devices != null) {
            Arrays.sort(devices,comparatorByProductionDate);
        }
    }

    private void filtrateBy(Device[] devices, String value, Params params){
        if (devices != null) {
            for (int i = 0; i < devices.length; i++) {
                if (devices[i] != null) {
                    String value1 = null;
                    switch (params) {
                        case TYPE:
                            value1 = devices[i].getType();
                            break;
                        case MANUFACTURER:
                            value1 = devices[i].getManufacturer();
                            break;
                        case MODEL:
                            value1 = devices[i].getModel();
                            break;
                    }
                    if ((value1 == null || value == null) ? value1 != value : value1.compareTo(value) != 0) {
                        devices[i] = null;
                    }
                }
            }
        }
    }

    @Override
    public void filtrateByType(Device[] devices, String type) {
        filtrateBy(devices,type, Params.TYPE);
    }

    @Override
    public void filtrateByManufacturer(Device[] devices, String manufacturer) {
        filtrateBy(devices,manufacturer,Params.MANUFACTURER);
    }

    @Override
    public void filtrateByModel(Device[] devices, String model) {
        filtrateBy(devices,model,Params.MODEL);
    }

    @Override
    public boolean isValidDeviceForInsertToRack(Device device) {
        return !(device == null || device.getIn() == 0);
    }

    @Override
    public boolean isValidDeviceForOutputToStream(Device device) {
         return !((device == null) ||
                 (device.getModel() != null && device.getModel().contains(STRING_NULL)  ) ||
                 (device.getType() != null && device.getType().contains(STRING_NULL)) ||
                 (device.getManufacturer() != null && device.getManufacturer().contains(STRING_NULL)) ||
                 (WifiRouter.class.isAssignableFrom(device.getClass()) &&
                         ((WifiRouter) device).getSecurityProtocol() != null &&
                         ((WifiRouter) device).getSecurityProtocol().contains(STRING_NULL)));
    }

    @Override
    public boolean isValidDeviceForWriteToStream(Device device) {
        return !((device == null) ||
                (device.getModel() != null && (device.getModel().contains(STRING_NULL) || device.getModel().contains(STRING_DELIMITER))) ||
                (device.getType() != null && (device.getType().contains(STRING_NULL) || device.getType().contains(STRING_DELIMITER))) ||
                (device.getManufacturer() != null && (device.getManufacturer().contains(STRING_NULL) || device.getManufacturer().contains(STRING_DELIMITER))) ||
                (WifiRouter.class.isAssignableFrom(device.getClass()) &&
                        ((WifiRouter) device).getSecurityProtocol() != null &&
                        (((WifiRouter) device).getSecurityProtocol().contains(STRING_NULL)
                        || ((WifiRouter) device).getSecurityProtocol().contains(STRING_DELIMITER))));
    }

    @Override
    public void outputDevice(Device device, OutputStream outputStream) throws IOException {
        if (device != null) {
            if (!isValidDeviceForOutputToStream(device)) {
                DeviceValidationException e = new DeviceValidationException("Invalid device for DeviceService.outputDevice");
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
                throw e;
            }
            if (outputStream == null) {
                IllegalArgumentException e = new IllegalArgumentException("The outputStream argument can not be null");
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
                throw e;
            }
            DataOutputStream out = new DataOutputStream(outputStream);
            out.writeUTF(device.getClass().getName());
            out.writeInt(device.getIn());
            out.writeUTF(device.getType() != null ? device.getType() : STRING_NULL);
            out.writeUTF(device.getModel() != null ? device.getModel() : STRING_NULL);
            out.writeUTF(device.getManufacturer() != null ? device.getManufacturer() : STRING_NULL);
            out.writeLong(device.getProductionDate() != null ? device.getProductionDate().getTime() : -1);
            if (Battery.class.isAssignableFrom(device.getClass())) {
                out.writeInt(((Battery) device).getChargeVolume());
            }
            if (Router.class.isAssignableFrom(device.getClass())) {
                out.writeInt(((Router) device).getDataRate());
            }
            if (Switch.class.isAssignableFrom(device.getClass())) {
                out.writeInt(((Switch) device).getNumberOfPorts());
            }
            if (WifiRouter.class.isAssignableFrom(device.getClass())) {
                out.writeUTF(((WifiRouter) device).getSecurityProtocol() != null ?
                        ((WifiRouter) device).getSecurityProtocol() : STRING_NULL);
            }
        }
    }

    @Override
    public Device inputDevice(InputStream inputStream) throws IOException, ClassNotFoundException {
        if (inputStream == null) {
            IllegalArgumentException e = new  IllegalArgumentException("The inputStream argument can not be null");
            LOGGER.log(Level.SEVERE,e.getMessage(),e);
            throw e;
        }

        DataInputStream in = new DataInputStream(inputStream);
        String className = in.readUTF();
        if (className.compareTo(RackServiceImpl.STRING_NULL) == 0 ) {
            return null;
        }
        Class clazz;
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
        Device device = null;
        if (Battery.class.isAssignableFrom(clazz)) {
            device = new Battery();
        }
        if (Router.class.isAssignableFrom(clazz)) {
            device = new Router();
        }
        if (Switch.class.isAssignableFrom(clazz)) {
            device = new Switch();
        }
        if (WifiRouter.class.isAssignableFrom(clazz)) {
            device = new WifiRouter();
        }
        if (device != null) {
            device.setIn(in.readInt());
            String type = in.readUTF();
            device.setType(type.compareTo(STRING_NULL) == 0 ? null : type);
            String model = in.readUTF();
            device.setModel(model.compareTo(STRING_NULL) == 0 ? null : model);
            String manufacturer = in.readUTF();
            device.setManufacturer(manufacturer.compareTo(STRING_NULL) == 0 ? null : manufacturer);
            Long productionDate = in.readLong();
            device.setProductionDate(productionDate == -1 ? null : new Date(productionDate));
            if (Battery.class.isAssignableFrom(clazz)) {
                ((Battery) device).setChargeVolume(in.readInt());
            }
            if (Router.class.isAssignableFrom(clazz)) {
                ((Router) device).setDataRate(in.readInt());
            }
            if (Switch.class.isAssignableFrom(clazz)) {
                ((Switch) device).setNumberOfPorts(in.readInt());
            }
            if (WifiRouter.class.isAssignableFrom(clazz)) {
                String securityProtocol = in.readUTF();
                ((WifiRouter) device).setSecurityProtocol(securityProtocol.compareTo(STRING_NULL) == 0 ? null : securityProtocol);
            }
        }
        return device;
    }

    @Override
    public void writeDevice(Device device, Writer writer) throws IOException {
        if (device != null) {
            if (!isValidDeviceForWriteToStream(device)) {
                DeviceValidationException e = new DeviceValidationException("Invalid device for DeviceService.writeDevice");
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
                throw e;
            }
            if (writer == null) {
                IllegalArgumentException e = new IllegalArgumentException("The writer argument can not be null");
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
                throw e;
            }

            writer.write(device.getClass().getName()+ STRING_NULL);

            StringBuilder devicesfields = new StringBuilder();
            devicesfields.append(Integer.toString(device.getIn()) + " " + STRING_DELIMITER);

            devicesfields.append(device.getType() != null ? " " + device.getType() : "").append(" " + STRING_DELIMITER);

            devicesfields.append(device.getModel() != null ? " " + device.getModel() : "").append(" " + STRING_DELIMITER);

            devicesfields.append(device.getManufacturer() != null ? " " + device.getManufacturer() : "").append(" " + STRING_DELIMITER);

            devicesfields.append(" ").append((device.getProductionDate() != null) ? (Long.toString(device.getProductionDate().getTime()))
                    : (Long.toString(-1))).append(" " + STRING_DELIMITER);

            if (Battery.class.isAssignableFrom(device.getClass())) {
                devicesfields.append(" ").append(Integer.toString(((Battery) device).getChargeVolume()));
            }
            if (Router.class.isAssignableFrom(device.getClass())) {
                devicesfields.append(" ").append(Integer.toString(((Router) device).getDataRate()));

                if (Switch.class.isAssignableFrom(device.getClass())) {
                    devicesfields.append(" " + STRING_DELIMITER);
                    devicesfields.append(" ").append(Integer.toString(((Switch) device).getNumberOfPorts()));
                }
                if (WifiRouter.class.isAssignableFrom(device.getClass())) {
                    devicesfields.append(" " + STRING_DELIMITER);
                    devicesfields.append(((WifiRouter) device).getSecurityProtocol() != null ?
                            " " + ((WifiRouter) device).getSecurityProtocol() : " ");
                }
            }

            writer.write(devicesfields.toString()+ STRING_NULL);
        }
    }


    @Override
    public Device readDevice(Reader reader) throws IOException, ClassNotFoundException {
        if (reader == null) {
            IllegalArgumentException e = new IllegalArgumentException("The reader argument can not be null");
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }

        String className = readLine(reader);
        if (className.compareTo("") == 0 ) {
            return null;
        }

        Class clazz;
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }

        Device device = null;
        if (Battery.class.isAssignableFrom(clazz)) {
            device = new Battery();
        }
        if (Router.class.isAssignableFrom(clazz)) {
            device = new Router();
        }
        if (Switch.class.isAssignableFrom(clazz)) {
            device = new Switch();
        }
        if (WifiRouter.class.isAssignableFrom(clazz)) {
            device = new WifiRouter();
        }
        if (device != null) {

            String s = readLine(reader);
            StringTokenizer deviceFields = new StringTokenizer(s,STRING_DELIMITER);
            device.setIn(
                    Integer.parseInt(deviceFields.nextToken().trim()));
            String type = deviceFields.nextToken();
            device.setType(type.length() == 1 ? null : type.substring(1, type.length() - 1));
            String model = deviceFields.nextToken();
            device.setModel(model.length() == 1 ? null : model.substring(1, model.length() - 1));
            String manufacturer = deviceFields.nextToken();
            device.setManufacturer(manufacturer.length() == 1 ? null : manufacturer.substring(1, manufacturer.length() - 1));
            Long productionDate = Long.parseLong(deviceFields.nextToken().trim());
            device.setProductionDate(productionDate == -1 ? null : new Date(productionDate));
            if (Battery.class.isAssignableFrom(clazz)) {
                ((Battery) device).setChargeVolume(Integer.parseInt(deviceFields.nextToken().trim()));
            }
            if (Router.class.isAssignableFrom(clazz)) {
                ((Router) device).setDataRate(
                        Integer.parseInt(
                                deviceFields.nextToken().replace(" ","")));
                if (Switch.class.isAssignableFrom(clazz)) {
                    ((Switch) device).setNumberOfPorts(Integer.parseInt(deviceFields.nextToken().replace(" ","")));
                }
                if (WifiRouter.class.isAssignableFrom(clazz)) {
                    String securityProtocol = deviceFields.nextToken();
                    ((WifiRouter) device).setSecurityProtocol(securityProtocol.length() == 1 ? null : securityProtocol.substring(1));
                }
            }
            return device;
        }
        return null;
    }

    protected String readLine(Reader reader) throws IOException {
        StringBuilder stringBuilder = new StringBuilder("");
        char c = (char)reader.read();
            while (Character.toString(c).compareTo(STRING_NULL) != 0) {
                stringBuilder.append(c);
                c = (char)reader.read();
            }
        return stringBuilder.toString();
    }

}
