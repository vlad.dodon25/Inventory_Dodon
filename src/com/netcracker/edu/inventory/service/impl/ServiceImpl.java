package com.netcracker.edu.inventory.service.impl;

import com.netcracker.edu.inventory.model.Device;
import com.netcracker.edu.inventory.service.DeviceService;
import com.netcracker.edu.inventory.service.RackService;
import com.netcracker.edu.inventory.service.Service;

/**
 * Created by vladd on 20.03.2017.
 */
public class ServiceImpl implements Service {

    @Override
    public DeviceService getDeviceService() {
        return new DeviceServiceImpl();
    }

    @Override
    public RackService getRackService() {
        return new RackServiceImpl();
    }

    @Override @Deprecated
    public void sortByIN(Device[] devices) {
        DeviceService deviceService = new DeviceServiceImpl();
        deviceService.sortByIN(devices);
    }

    @Override @Deprecated
    public void sortByProductionDate(Device[] devices) {
        DeviceService deviceService = new DeviceServiceImpl();
        deviceService.sortByProductionDate(devices);
    }

    @Override @Deprecated
    public void filtrateByType(Device[] devices, String type) {
        DeviceService deviceService = new DeviceServiceImpl();
        deviceService.filtrateByType(devices,type);
    }

    @Override @Deprecated
    public void filtrateByManufacturer(Device[] devices, String manufacturer) {
        DeviceService deviceService = new DeviceServiceImpl();
        deviceService.filtrateByManufacturer(devices,manufacturer);
    }

    @Override @Deprecated
    public void filtrateByModel(Device[] devices, String model) {
        DeviceService deviceService = new DeviceServiceImpl();
        deviceService.filtrateByModel(devices, model);
    }

    @Override @Deprecated
    public boolean isValidDeviceForInsertToRack(Device device) {
        DeviceService deviceService = new DeviceServiceImpl();
        return deviceService.isValidDeviceForInsertToRack(device);
    }
}
