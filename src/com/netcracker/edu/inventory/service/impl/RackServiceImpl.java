package com.netcracker.edu.inventory.service.impl;

import com.netcracker.edu.inventory.model.Device;
import com.netcracker.edu.inventory.model.Rack;
import com.netcracker.edu.inventory.model.impl.*;
import com.netcracker.edu.inventory.service.DeviceService;
import com.netcracker.edu.inventory.service.RackService;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.*;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by vladd on 17.04.2017.
 */
class RackServiceImpl implements RackService{

    protected static Logger LOGGER = Logger.getLogger(RackServiceImpl.class.getName());

    protected  static final String STRING_NULL = "\n";

    RackServiceImpl() {}
    @Override
    public void outputRack(Rack rack, OutputStream outputStream) throws IOException {
        if (rack!= null) {
            if (outputStream == null) {
                IllegalArgumentException e = new IllegalArgumentException("The outputStream argument can not be null");
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
                throw e;
            }

            DataOutputStream out = new DataOutputStream(outputStream);
            out.writeInt(rack.getSize());
            out.writeUTF(rack.getTypeOfDevices().getName());

            DeviceService deviceService = new DeviceServiceImpl();

            for (int i = 0; i < rack.getSize(); i++) {
                if (rack.getDevAtSlot(i) != null) {
                    deviceService.outputDevice(rack.getDevAtSlot(i), outputStream);
                } else {
                    out.writeUTF(STRING_NULL);
                }
            }
        }
    }

    @Override
    public Rack inputRack(InputStream inputStream) throws IOException, ClassNotFoundException {
        if (inputStream == null) {
            IllegalArgumentException e = new  IllegalArgumentException("The inputStream argument can not be null");
            LOGGER.log(Level.SEVERE,e.getMessage(),e);
            throw e;
        }

        DataInputStream in = new DataInputStream(inputStream);
        int size = in.readInt();
        String typeOfDevisesName = in.readUTF();

        Class typeOfDevises;
        try {
            typeOfDevises = Class.forName(typeOfDevisesName);
        } catch (ClassNotFoundException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
        Rack rack = new RackArrayImpl(size,typeOfDevises);

        DeviceService deviceService = new DeviceServiceImpl();
        Device device;
            for (int i = 0; i < size ; i++) {
                device = deviceService.inputDevice(inputStream);
                if (device != null) {
                    rack.insertDevToSlot( device, i);
                }
            }
        return rack;
    }

    @Override
    public void writeRack(Rack rack, Writer writer) throws IOException {
        if (rack != null) {
            if (writer == null) {
                IllegalArgumentException e = new IllegalArgumentException("The writer argument can not be null");
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
                throw e;
            }

            writer.write(Integer.toString(rack.getSize()) + " " + rack.getTypeOfDevices().getName() + STRING_NULL);
            DeviceService deviceService = new DeviceServiceImpl();
            for (int i = 0; i < rack.getSize(); i++) {
                if (rack.getDevAtSlot(i) != null) {
                    deviceService.writeDevice(rack.getDevAtSlot(i), writer);
                } else {
                    writer.write(STRING_NULL);
                }
            }
        }
    }

    @Override
    public Rack readRack(Reader reader) throws IOException, ClassNotFoundException {
        if (reader == null) {
            IllegalArgumentException e = new  IllegalArgumentException("The reader argument can not be null");
            LOGGER.log(Level.SEVERE,e.getMessage(),e);
            throw e;
        }

        StringTokenizer rackfields = new StringTokenizer(new DeviceServiceImpl().readLine(reader)," ");
        int size = Integer.parseInt(rackfields.nextToken());
        String typeOfDevisesName = rackfields.nextToken();

        Class typeOfDevises;
        try {
            typeOfDevises = Class.forName(typeOfDevisesName);
        } catch (ClassNotFoundException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }

        Rack rack = new RackArrayImpl(size,typeOfDevises);

        DeviceService deviceService = new DeviceServiceImpl();
        Device device;
        for (int i = 0; i < size ; i++) {
            device = deviceService.readDevice(reader);
            if (device != null) {
                rack.insertDevToSlot( device, i);
            }
        }
        return rack;
    }
}
