package com.netcracker.edu.inventory.exception;

import com.netcracker.edu.inventory.model.Device;


/**
 * Created by vladd on 27.03.2017.
 */
public class DeviceValidationException extends RuntimeException {

    protected static String defaultMessage = "Device is not valid for operation";
    protected Device objects;

    public DeviceValidationException(String operation, Device objects) {
        this(operation);
        this.objects = objects;
    }

    public DeviceValidationException(String operation) {
        super((operation != null) ? defaultMessage + ": " + operation : defaultMessage);
    }

    public DeviceValidationException() {
        super();
    }


    public Device getObjects() {
        return objects;
    }
}
