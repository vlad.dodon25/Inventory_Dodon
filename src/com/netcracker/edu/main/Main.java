package com.netcracker.edu.main;

public class Main {

    public static void main(String[] args) {
        sort(args);
        print(args);

        StringBuilder fields = new StringBuilder( "0 | 1 | 2 | 3 | 4 | 5");
        String[] deviceFields = new String[6];
        for (int i = 0; i < 5; i++) {
            int index = fields.indexOf("|");
            if (index > 0)
            deviceFields[i] = "";
        }
        for (int i = 0; i < deviceFields.length; i++)
        System.out.println(deviceFields[i]);


    }

    public static void print(String[] args) {
        for (int i = 0; i < args.length; i++) {
            System.out.println(args[i]);
        }
    }

    public static void sort(String[] args){
        for (int i = args.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (args[j].compareTo(args[j + 1]) > 0) {
                    String t = args[j];
                    args[j] = args[j + 1];
                    args[j + 1] = t;
                }
            }
        }
    }
}
